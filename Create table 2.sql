use prekes;

CREATE TABLE prekes (
PrekesID int auto_increment,
Kaina FLOAT(5,2),
MatVnt varchar(100),
Pavadinimas varchar(200),
Barkodas char(11)
);

ALTER TABLE prekes ADD PrekesID INT NOT NULL PRIMARY KEY AUTO_INCREMENT;